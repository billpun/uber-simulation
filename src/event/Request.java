/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package event;

import base.DistanceInfo;
import base.Driver;
import base.Location;
import base.Rider;

/**
 *
 * @author cpun1
 */
public class Request extends Event {

    public int id;
    public Rider rider;
    public Driver driver;

    public Location driver_location;
    public Location origin;
    public Location destination;

    public DistanceInfo pickup_info;
    public double charges;
    public double miles;

    public Request() {
        rider = new Rider();
        driver = new Driver();
        driver_location = new Location();
        origin = new Location();
        destination = new Location();
        pickup_info = new DistanceInfo();
        charges = 0;
        miles = 0;
    }

    public Request(String[] s) {

        int i = 0;
        this.id = Integer.valueOf(s[i++].trim());
        
        this.rider = new Rider();
        this.rider.id = Integer.valueOf(s[i++].trim());
        
        this.driver = new Driver();
        this.driver.id = Integer.valueOf(s[i++].trim());
        
        this.driver_location = new Location();
        this.driver_location.latitude = Double.valueOf(s[i++].trim());
        this.driver_location.longitude = Double.valueOf(s[i++].trim());
        
        this.origin = new Location();
        this.origin.latitude = Double.valueOf(s[i++].trim());
        this.origin.longitude = Double.valueOf(s[i++].trim());
        this.origin.datetime = Long.valueOf(s[i++].trim());
        
        this.destination = new Location();
        this.destination.latitude = Double.valueOf(s[i++].trim());
        this.destination.longitude = Double.valueOf(s[i++].trim());
        this.destination.datetime = Long.valueOf(s[i++].trim());
        
        this.pickup_info = new DistanceInfo();
        this.pickup_info.travel_time = Long.valueOf(s[i++].trim());
        this.pickup_info.miles = Double.valueOf(s[i++].trim());
        
        this.charges = Double.valueOf(s[i++].trim());
        this.miles = Double.valueOf(s[i++].trim());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        char dlim = '\t';
        sb.append(this.id).append(dlim);
        sb.append(this.rider.id).append(dlim);

        sb.append(this.driver.id).append(dlim);
        sb.append(this.driver_location.latitude).append(dlim);
        sb.append(this.driver_location.longitude).append(dlim);

        sb.append(this.origin.latitude).append(dlim);
        sb.append(this.origin.longitude).append(dlim);
        sb.append(this.origin.datetime).append(dlim);

        sb.append(this.destination.latitude).append(dlim);
        sb.append(this.destination.longitude).append(dlim);
        sb.append(this.destination.datetime).append(dlim);

        sb.append(this.pickup_info.travel_time).append(dlim);
        sb.append(this.pickup_info.miles).append(dlim);

        sb.append(this.charges).append(dlim);
        sb.append(this.miles);
        return sb.toString();
    }
}
