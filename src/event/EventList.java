/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package event;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cpun1
 */
public class EventList {

    private final LinkedList<Event> events;
    private int current_position;

    public EventList() {
        current_position = 0;
        events = new LinkedList();
    }

    /* add an event to the time-ordered event list */
    public void add(Event e) {
        if (events.isEmpty()) {
            events.add(e);
        } else {
            for (int i = 0; i < events.size() - 1; ++i) {
                
                // if the event time is in between the two buckets, 
                // then insert the event in between
                if (e.event_time < events.get(i + 1).event_time
                        && e.event_time >= events.get(i).event_time) {
                    events.add(i + 1, e);
                    return;
                }
            }
            
            // if no buckets are applicable,
            /// the event should be placed at the end of the list
            events.add(events.size(), e);
        }
    }

    /* get next event */
    public Event next() {
        Event e = events.get(this.current_position);
        this.current_position++;
        return e;
    }

    /* get all events */
    public List<Event> getAll() {
        return this.events;
    }

    /* check if there are more events */
    public boolean hasNext() {
        return (this.current_position < events.size());
    }
}
