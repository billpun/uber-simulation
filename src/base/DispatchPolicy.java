/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import event.Request;
import java.util.List;
import java.util.logging.Level;
import misc.Configurer;
import misc.Enums;
import misc.Utilities;
import org.apache.commons.math3.random.MersenneTwister;
import uber.Uber;

/**
 *
 * @author cpun1
 */
public class DispatchPolicy {

    private final int dispatch_policy;
    private final MersenneTwister mt;

    public DispatchPolicy(MersenneTwister mt) {
        this.mt = mt;
        this.dispatch_policy = Configurer.dispatch_policy;
    }

    public Driver findDriver(Request request, List<Driver> drivers) {
        Driver driver = null;
        
        // if dispatch policy is to find the nearest driver
        if (dispatch_policy == Enums.POLICY.nearest.ordinal()) {
            driver = findNearestDriver(request, drivers);
        }
        
        return driver;
    }

    private Driver findNearestDriver(Request request, List<Driver> drivers) {
        Driver driver = null;
        try {
            double miles = 999999;

            // for each driver location
            for (Driver _driver : drivers) {

                // if request time is later than the release time of the driver
                // implying that the driver is available at the request time
                if (request.origin.datetime > _driver.release_location.datetime) {

                    // simulate driver's current location
                    _driver.simulateCurrentLocation(request, mt);

                    // get distance between the request location and the delivery location of last request
                    double _miles = Utilities.getDistanceInfo(request.origin, _driver.current_location).miles;

                    // if maximize pickup distance has not been exceeded
                    if (Configurer.max_pickup_distance > _miles) {

                        // and it is closer to the request
                        if (miles > _miles) {
                            
                            // select that driver
                            driver = _driver;
                            
                            // update the minimum miles
                            miles = _miles;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, "{0}: {1}", new Object[]{request.id, e.getMessage()});
        }
        return driver;
    }

}
