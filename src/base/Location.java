/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import java.util.logging.Level;
import misc.Utilities;
import org.apache.commons.math3.random.MersenneTwister;
import uber.Uber;

/**
 *
 * @author cpun1
 */
public class Location {

    public double latitude;
    public double longitude;
    public long datetime;

    public Location() {
    }

    // a copy constructor
    public Location(Location loc) {
        this.latitude = loc.latitude;
        this.longitude = loc.longitude;
        this.datetime = loc.datetime;
    }

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCoordinate() {
        return this.latitude + ", " + this.longitude;
    }

    // convert long datetime to string datetime
    public String getDateTimeString() {
        return Utilities.long2date(datetime);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String dlim = ", ";
        sb.append(this.latitude).append(dlim);
        sb.append(this.longitude).append(dlim);
        sb.append(Utilities.long2date(datetime));
        return sb.toString();
    }
    
    // randomize location based on a latitude upper bound
    public Location getRandomLocation(double latitude_ub, MersenneTwister _mt) {
        Location loc = null;
        try {
            // convert distance in miles to upper bounds in latitude and longitude
            Location upper_bound = Utilities.getLatLngInMiles(this, latitude_ub);

            // repeat until a valid street address is found
            do {
                // randomize new location
                loc = new Location(
                        this.latitude + (_mt.nextDouble() - 0.5) * upper_bound.latitude,
                        this.longitude + (_mt.nextDouble() - 0.5) * upper_bound.longitude);
            } while (Utilities.isStreetAddress(loc) == false);

        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return loc;
    }
}
