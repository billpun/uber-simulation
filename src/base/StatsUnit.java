/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import misc.Utilities;

/**
 *
 * @author cpun1
 */
public class StatsUnit {

    public double mean;
    public double std;

    public StatsUnit() {
        this.mean = 0;
        this.std = 0;
    }

    @Override
    public String toString() {
        double _mean = Utilities.round(this.mean, 2);
        double _std = Utilities.round(this.std, 2);
        return _mean + ", " + _std;
    }
}
