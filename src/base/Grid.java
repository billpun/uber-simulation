/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import event.Request;

/**
 *
 * @author cpun1
 */
public class Grid {

    public int id;
    public double left_latitude;
    public double right_latitude;
    public double top_longitude;
    public double bottom_longitude;

    //
    public Grid() {
    }

    // check if the request's origin falls into this grid
    public boolean isOriginInGrid(Request request) {
        if (this.left_latitude <= request.origin.latitude
                && this.right_latitude > request.origin.latitude
                && this.bottom_longitude <= request.origin.longitude
                && this.top_longitude > request.origin.longitude) {
            return true;
        }
        return false;
    }

    // check if the request's destination falls into this grid
    public boolean isDestinInGrid(Request request) {
        if (this.left_latitude <= request.destination.latitude
                && this.right_latitude > request.destination.latitude
                && this.bottom_longitude <= request.destination.longitude
                && this.top_longitude > request.destination.longitude) {
            return true;
        }
        return false;
    }
}
