/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import event.Request;
import misc.Configurer;
import org.apache.commons.math3.random.MersenneTwister;

/**
 *
 * @author cpun1
 */
public class Driver {

    public int id;
    public String first_name;
    public String last_name;

    // delivery location and datetime of last request
    public Location release_location;
    
    // current location of the driver
    public Location current_location;

    // constructor
    public Driver() {
        id = -1;
        first_name = "";
        last_name = "";
        release_location = new Location();
        current_location = new Location();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        char dlim = '\t';
        sb.append(this.id).append(dlim);
        sb.append(this.first_name).append(dlim);
        sb.append(this.last_name);
        return sb.toString();
    }

    // simulate current location of the driver 
    public void simulateCurrentLocation(Request request, MersenneTwister mt) {

        // time between driver's last release and the current request
        long time_delta = request.origin.datetime - this.release_location.datetime;

        // maximum distance in miles the driver could have traveled in time_delta
        double max_distance = Math.min(Configurer.latitude_margin * 2, time_delta * 0.42 / 60000 / 2);

        // randomize driver's location based on the last released location
        this.current_location = this.release_location.getRandomLocation(max_distance, mt);
    }
}
