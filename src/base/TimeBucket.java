/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import misc.Configurer;
import org.apache.commons.math3.random.MersenneTwister;

/**
 *
 * @author cpun1
 */
public class TimeBucket {
    
    private final long ONE_MINUTE_IN_MILLIS;

    public long from;
    public long to;
    public StatsUnit stats;

    // constructor
    public TimeBucket() {
        this.ONE_MINUTE_IN_MILLIS = Configurer.ONE_MINUTE_IN_MILLIS;
        stats = new StatsUnit();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.toString();
    }

    // return randomize time in milliseconds
    public long getRandomizedTime(MersenneTwister mt) {
        long delta = Math.round((this.to - this.from) * mt.nextDouble() / this.ONE_MINUTE_IN_MILLIS);
        return this.from + delta * this.ONE_MINUTE_IN_MILLIS;
    }
}
