/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

/**
 *
 * @author cpun1
 */
public class Rider {

    public int id;
    public String first_name;
    public String last_name;

    public Rider() {
        id = -1;
        first_name = "";
        last_name = "";
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        char dlim = '\t';
        sb.append(this.id).append(dlim);
        sb.append(this.first_name).append(dlim);
        sb.append(this.last_name);
        return sb.toString();
    }
}
