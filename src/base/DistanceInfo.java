/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

/**
 *
 * @author cpun1
 */
public class DistanceInfo {
    
    // travel distance in miles
    public double miles;
    
    // travel time in milliseconds
    public long travel_time;
    
    // constructor
    public DistanceInfo() {    
    }
}
