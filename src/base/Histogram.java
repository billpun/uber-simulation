/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import java.util.ArrayList;
import java.util.List;
import misc.Configurer;
import org.apache.commons.math3.random.MersenneTwister;

/**
 *
 * @author cpun1
 */
public class Histogram {

    // histogram bins
    public List<TimeBucket> time_bkts;
    
    // constructor
    public Histogram() {
        
        // initialize time buckets
        this.time_bkts = new ArrayList();
        
        // determine time increment
        int size = Configurer.number_of_time_bkts;
        int incr = (int) (1440.0 * 60000.0 / (1.0 * size));
        
        // generate time buckets
        for (int i = 0; i < size; ++i) {
            TimeBucket time_bkt = new TimeBucket();
            time_bkt.from = i * incr;
            time_bkt.to = (i + 1) * incr;
            time_bkts.add(time_bkt);
        }
        time_bkts.get(size - 1).to = 1440 * 60000 + 1;
    }

    // add number of requests to corresponding time bucket
    public void add(long time, double n) {
        for (TimeBucket bkt : time_bkts) {
            if (bkt.from <= time && bkt.to > time) {
                bkt.stats.mean += n;
                break;
            }
        }
    }

    // get a time bucket
    public TimeBucket get(int i) {
        return (i < time_bkts.size()) ? time_bkts.get(i) : null;
    }

    // get number of time buckets
    public int size() {
        return time_bkts.size();
    }

    // get random time from the randomly assigned time bucket
    public long getRandomTime(MersenneTwister mt) {
        try {
            int size = time_bkts.size();
            if (size == 1) {
                return time_bkts.get(0).getRandomizedTime(mt);
            }
            double total = 0;
            double[] accum = new double[size + 1];
            double last_num = 0;
            for (int i = 0; i < size; ++i) {
                total += time_bkts.get(i).stats.mean;
                accum[i + 1] = last_num + time_bkts.get(i).stats.mean;
                last_num = accum[i + 1];
            }
            double r = mt.nextDouble();
            for (int i = 0; i < size; ++i) {
                double _r = r * total;
                if (accum[i] <= _r && _r < accum[i + 1]) {
                    return time_bkts.get(i).getRandomizedTime(mt);
                }
            }
            return time_bkts.get(size - 1).getRandomizedTime(mt);
        } catch (Exception e) {
            System.err.println("Functions: multinomial: " + e.getMessage());
            return -1;
        }
    }
}
