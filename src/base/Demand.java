/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import event.Request;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cpun1
 */
public class Demand {

    private String id;
    
    // origin grid
    public Grid from;
    public Location from_centroid;
    public Location from_spread;

    // destination grid
    public Grid to;
    public Location to_centroid;
    public Location to_spread;

    // mean and std of the number of requests
    public StatsUnit stats;

    // a list of requests for this grid pair
    public List<Request> requests;

    // request distribution across hours
    public Histogram histogram;

    // constructor
    public Demand() {
        this.from_centroid = new Location();
        this.from_spread = new Location();
        this.to_centroid = new Location();
        this.to_spread = new Location();
        this.stats = new StatsUnit();
        this.requests = new ArrayList();
        this.histogram = new Histogram();
    }

    public void setID() {
        this.id = from.id + " " + to.id;
    }

    public String getID() {
        return this.id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        char dlim = '\t';
        sb.append(this.id).append(dlim);
        sb.append(this.from.id).append(dlim);
        sb.append(this.from_centroid.latitude).append(dlim);
        sb.append(this.from_centroid.longitude).append(dlim);
        sb.append(this.from_spread.latitude).append(dlim);
        sb.append(this.from_spread.longitude).append(dlim);
        sb.append(this.to.id).append(dlim);
        sb.append(this.to_centroid.latitude).append(dlim);
        sb.append(this.to_centroid.longitude).append(dlim);
        sb.append(this.to_spread.latitude).append(dlim);
        sb.append(this.to_spread.longitude).append(dlim);
        sb.append(this.stats.mean).append(dlim);
        sb.append(this.stats.std);
        return sb.toString();
    }
}
