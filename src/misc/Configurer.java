/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import au.com.bytecode.opencsv.CSVWriter;
import base.Location;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.Properties;
import java.util.logging.Level;
import uber.Uber;

/**
 *
 * @author cpun1
 */
public class Configurer {
    
    public static final long ONE_MINUTE_IN_MILLIS = 60000;

    // output files
    public static final String time_dist_file = "./input/time_distribution.txt";
    public static final String request_file = "./input/requests.txt";
    public static final String rider_file = "./input/riders.txt";
    public static final String driver_file = "./input/drivers.txt";

    //
    public static String datetime_format = "MM/dd/yyyy HH:mm:SS";
    //
    public static int run_mode = 0;
    //
    public static int number_of_requests = 500;
    public static Location coord0 = new Location(40.7484, -73.9857);
    public static double latitude_margin = 20;
    //
    public static double max_pickup_distance = 5;
    public static boolean use_google = false;
    //
    public static int number_of_grids = 2;
    public static int number_of_time_bkts = 24;
    public static int number_of_streams = 100;
    // 
    public static int dispatch_policy = 0;
    //
    public static String filename = "uber.config";
    
    public Configurer() {
    }
    
    public void run() {
        try {
            // generate the configuration file if not exists
            if (!new File(filename).exists()) {
                this.save();
            }
            
            Timing timer = new Timing();

            // read parameters from tera.config
            Properties property = new Properties();
            FileInputStream in = new FileInputStream(this.filename);
            property.load(in);

            // get datetime format
            Configurer.datetime_format = property.getProperty("datetime_format").trim();

            // load parameters
            Configurer.run_mode = Integer.valueOf(property.getProperty("run_mode").trim());

            // get number of requests
            Configurer.number_of_requests = Integer.valueOf(property.getProperty("number_of_requests").trim());

            // get initial coordinates
            String[] temp = property.getProperty("latlng").trim().split(",");
            Configurer.coord0 = new Location(Double.valueOf(temp[0]), Double.valueOf(temp[1]));

            // get latitude margin
            Configurer.latitude_margin = Double.valueOf(property.getProperty("latitude_margin").trim());

            // get maximum pickup distance
            Configurer.max_pickup_distance = Double.valueOf(property.getProperty("max_pickup_distance").trim());

            // get use google indicator
            Configurer.use_google = property.getProperty("use_google").trim().equals("true");

            // get number of grids on latitude
            Configurer.number_of_grids = Integer.valueOf(property.getProperty("number_of_grids").trim());

            // get number of time buckets
            Configurer.number_of_time_bkts = Integer.valueOf(property.getProperty("number_of_time_bkts").trim());

            // get number of streams
            Configurer.number_of_streams = Integer.valueOf(property.getProperty("number_of_streams").trim());

            // get dispatch_policy
            Configurer.dispatch_policy = Integer.valueOf(property.getProperty("dispatch_policy").trim());

            // print out 
            Uber.LOGGER.log(Level.INFO, "Finished reading configurations from {0}. Took {1}s.",
                    new Object[]{Configurer.filename, timer.getSec()});
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }
    
    public static void save() {
        try {
            CSVWriter out = new CSVWriter(new FileWriter(Configurer.filename), '\n',
                    CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);

            // write datetime format
            out.writeNext("// inpute datetime format");
            out.writeNext("datetime_format = " + Configurer.datetime_format);
            out.writeNext();

            // write run mode
            out.writeNext("// run mode");
            out.writeNext("// 0 - generate input");
            out.writeNext("// 1 - simulate requests");
            out.writeNext("// 2 - simulate drivers");
            out.writeNext("// 3 - compute KPIs");
            out.writeNext("run_mode = " + Configurer.run_mode);
            out.writeNext();

            // write input configuration
            out.writeNext("// --- generate input ---");

            // write number of requests
            out.writeNext("// number of requests");
            out.writeNext("number_of_requests = " + Configurer.number_of_requests);
            out.writeNext();

            // write start latitude and longitude
            out.writeNext("// start latitude and longitude");
            out.writeNext("latlng = " + Configurer.coord0.getCoordinate());
            out.writeNext();

            // write latitude margin from latlng_0
            out.writeNext("// latitude margin from latlng in miles");
            out.writeNext("latitude_margin = " + Configurer.latitude_margin);
            out.writeNext();

            // write maximumn pickup distance
            out.writeNext("// maximum pickup distance");
            out.writeNext("max_pickup_distance = " + Configurer.max_pickup_distance);
            out.writeNext();

            // write use google indicator
            out.writeNext("// use google to valid distance and street address");
            out.writeNext("use_google = " + Configurer.use_google);
            out.writeNext();

            // write number of grids on latitude
            out.writeNext("// -- generate grids");
            out.writeNext("// number of grids on latitude");
            out.writeNext("number_of_grids = " + Configurer.number_of_grids);
            out.writeNext();

            // write number of time buckets
            out.writeNext("// -- compute histogram");
            out.writeNext("// number of time buckets");
            out.writeNext("number_of_time_bkts = " + Configurer.number_of_time_bkts);
            out.writeNext();

            // write number of streams
            out.writeNext("// -- simulation");
            out.writeNext("// number of streams");
            out.writeNext("number_of_streams = " + Configurer.number_of_streams);
            out.writeNext();

            // write dispatch dispatch_policy
            out.writeNext("// driver dispatch policy");
            out.writeNext("// 0 - nearest");
            out.writeNext("dispatch_policy = " + Configurer.dispatch_policy);
            out.writeNext();
            
            out.close();
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }
}
