/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * @date    12/17/2013
 * @author  Bill Pun
 */
public class Timing {

    long startTime;
    long duration;

    public Timing() {
        startTime = System.nanoTime();
    }

    public void reset() {
        startTime = System.nanoTime();
    }

    public String getTime() {
        duration = System.nanoTime() - startTime;
        return this.fc_DateTime(duration);
    }

    public String getHr() {
        duration = System.nanoTime() - startTime;
        return String.valueOf(TimeUnit.NANOSECONDS.toHours(duration));
    }

    public String getMin() {
        duration = System.nanoTime() - startTime;
        return String.valueOf(TimeUnit.NANOSECONDS.toMinutes(duration));
    }

    public String getSec() {
        duration = System.nanoTime() - startTime;
        return String.valueOf(TimeUnit.NANOSECONDS.toSeconds(duration));
    }

    public void printTime() {
        duration = System.nanoTime() - startTime;
        System.out.println(this.fc_DateTime(duration));
    }

    public void printHr() {
        duration = System.nanoTime() - startTime;
        System.out.println(TimeUnit.NANOSECONDS.toHours(duration));
    }

    public void printMin() {
        duration = System.nanoTime() - startTime;
        System.out.println(TimeUnit.NANOSECONDS.toMinutes(duration));
    }

    public void printSec() {
        duration = System.nanoTime() - startTime;
        System.out.println(TimeUnit.NANOSECONDS.toSeconds(duration));
    }

    private String fc_DateTime(long l_NanoSec) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return (dateFormat.format(new Date(l_NanoSec)));
    }
}
