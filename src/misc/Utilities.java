/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import base.Location;
import base.DistanceInfo;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.math3.random.MersenneTwister;
import org.json.JSONArray;
import org.json.JSONObject;
import uber.Uber;

/**
 * @date 12/17/2013
 * @author Bill Pun
 */
public class Utilities {

    public static String addFileTile(String dir) {
        if (dir.charAt(dir.length() - 1) != '/') {
            return (dir + '/');
        }
        return (dir);
    }

    public static void removeFiles(String dir) {
        File file = new File(dir);
        String[] myFiles;
        if (file.isDirectory()) {
            myFiles = file.list();
            for (int i = 0; i < myFiles.length; i++) {
                File myFile = new File(file, myFiles[i]);
                myFile.delete();
            }
        }
    }

    public static void removeSimilarFiles(String dir, String name) {
        ArrayList<String> files = Utilities.getAllFileNames(dir, false);
        for (String f : files) {
            if (f.split("\\.")[0].equals(name)) {
                (new File(dir + f)).delete();
            }
        }
    }

    public static void moveFiles(String from, String to, List<String> filenames) throws IOException {
        for (String fn : filenames) {
            File afile = new File(from + "/" + fn);
            File bfile = new File(to + "/" + fn);

            InputStream inStream = new FileInputStream(afile);
            OutputStream outStream = new FileOutputStream(bfile);

            byte[] buffer = new byte[1024];

            int length;

            //copy the file content in bytes 
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            inStream.close();
            outStream.close();
        }
    }

    public static List<String> readFile(String filename) throws FileNotFoundException, IOException {
        File file = new File(filename);
        BufferedReader in = new BufferedReader(new FileReader(file));
        List<String> out = new ArrayList();
        String text;
        while ((text = in.readLine()) != null) {
            out.add(text);
        }
        return out;
    }

    public static String readFileIntoLine(String filename) throws IOException {
        StringBuilder sb = new StringBuilder();
        List<String> lines = Utilities.readFile(filename);
        for (String line : lines) {
            sb.append(line).append(" ");
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public static void writeFile(List<String> content, String filename) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        for (String s : content) {
            out.write(s);
            out.newLine();
        }
        out.close();
    }

    public static void writeFile(String content, String filename) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filename));
        out.write(content);
        out.close();
    }

    public static ArrayList<String> getAllFileNames(String path, boolean getFullPath) {
        ArrayList<String> out = new ArrayList();
        File folder = new File(path);
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                if (getFullPath) {
                    out.add(fileEntry.getPath());
                } else {
                    out.add(fileEntry.getName());
                }
            } else {
                out.addAll(getAllFileNames(fileEntry.getPath(), getFullPath));
            }
        }
        return out;
    }

    public static double round(double unrounded, int precision) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, BigDecimal.ROUND_HALF_UP);
        return rounded.doubleValue();
    }

    public static String cleanseDir(String... dirs) {
        StringBuilder sb = new StringBuilder();
        for (String dir : dirs) {
            if (dir.endsWith("/")) {
                sb.append(dir);
            } else {
                sb.append(dir).append("/");
            }
        }
        return sb.toString();
    }

    public static void createFolder(String directory_name) {
        new File(directory_name).mkdirs();
    }

    public static String getString(ResultSet rs, int i) throws SQLException {
        String s = rs.getString(i);
        return (s == null) ? "" : s.trim();
    }

    public static long date2long(String date) {
        long datetime = -1;
        try {
            SimpleDateFormat f = new SimpleDateFormat(Configurer.datetime_format);
            Date d = f.parse(date);
            datetime = d.getTime();
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return datetime;
    }

    public static String long2date(long date) {
        String datetime = null;
        try {
            Date d = new Date(date);
            SimpleDateFormat df2 = new SimpleDateFormat(Configurer.datetime_format);
            datetime = df2.format(d);
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return datetime;
    }

    public static boolean bernoulli(double prob, MersenneTwister mt) {
        try {
            return (mt.nextDouble() <= prob);
        } catch (Exception e) {
            System.err.println("Functions: bernoulli: " + e.getMessage());
            return false;
        }
    }

    public static String multinomial(String[] values, MersenneTwister mt) {
        int size = values.length;
        if (size == 0) {
            return null;
        }

        String value = values[size - 1];
        if (size == 1) {
            return value;
        }

        double r = mt.nextDouble();
        double inc = 1 / (1.0 * size);
        for (int i = 1; i <= size; ++i) {
            if (r < i * inc) {
                value = values[i - 1];
                break;
            }
        }

        return value;
    }

    public static int multinomial(List<Double> values, MersenneTwister mt) {
        try {
            int size = values.size();
            if (size == 1) {
                return 0;
            }
            double total = 0;
            double[] accum = new double[size + 1];
            double last_num = 0;
            for (int i = 0; i < size; ++i) {
                total += values.get(i);
                accum[i + 1] = last_num + values.get(i);
                last_num = accum[i + 1];
            }
            double r = mt.nextDouble();
            for (int i = 0; i < size; ++i) {
                double _r = r * total;
                if (accum[i] <= _r && _r < accum[i + 1]) {
                    return i;
                }
            }
            return size - 1;
        } catch (Exception e) {
            System.err.println("Functions: multinomial: " + e.getMessage());
            return -1;
        }
    }

    /* check if the address is a street address */
    public static boolean isStreetAddress(final Location coord) {

        boolean use_google = Configurer.use_google;

        boolean valid_address = false;

        if (use_google) {
            // use google map service to valid address
            String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="
                    + coord.latitude + "," + coord.longitude + "&sensor=false";

            try {
                InputStream is = new URL(url).openStream();
                try {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sb = new StringBuilder();
                    int cp;
                    while ((cp = rd.read()) != -1) {
                        sb.append((char) cp);
                    }
                    JSONObject json = new JSONObject(sb.toString());
                    JSONArray a = (JSONArray) json.get("results");
                    json = a.getJSONObject(0);
                    a = (JSONArray) json.get("types");
                    String address_type = a.getString(0);
                    if ("street_address".equals(address_type)) {
                        valid_address = true;
                    }
                } catch (Exception ex) {
                    valid_address = true;
                }
                is.close();
            } catch (Exception e) {
                Uber.LOGGER.log(Level.SEVERE, e.getMessage());
            }
        } else {
            valid_address = true;
        }
        
        return valid_address;
    }

    /* get distance between two locations from google */
    public static DistanceInfo getDistanceInfo(final Location orig, final Location dest) {

        boolean use_google = Configurer.use_google;

        DistanceInfo tm = null;

        if (use_google) {

            String url = "http://maps.googleapis.com/maps/api/distancematrix/json?&origins=" + orig.latitude + "," + orig.longitude
                    + "&destinations=" + dest.latitude + "," + dest.longitude + "&sensor=false";
            try {
                InputStream is = new URL(url).openStream();
                try {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sb = new StringBuilder();
                    int cp;
                    while ((cp = rd.read()) != -1) {
                        sb.append((char) cp);
                    }
                    JSONObject json = new JSONObject(sb.toString());
                    JSONArray a = json.getJSONArray("rows");
                    json = a.getJSONObject(0);
                    a = json.getJSONArray("elements");
                    json = a.getJSONObject(0);
                    JSONObject distance_json = json.getJSONObject("distance");
                    double miles = distance_json.getInt("value") * 0.62137 / 1000;

                    JSONObject duration_json = json.getJSONObject("duration");
                    long travel_time = duration_json.getLong("value") * 1000;

                    tm = new DistanceInfo();
                    tm.miles = miles;
                    tm.travel_time = travel_time;
                } catch (Exception ex) {
                    use_google = false;
                }
                is.close();
            } catch (Exception e) {
                Uber.LOGGER.log(Level.SEVERE, e.getMessage());
            }
        }
        if (use_google == false) {
            // approximate if we have trouble with google
            // create an instance
            tm = new DistanceInfo();

            // get approximate distance 
            tm.miles = getHaversine(orig, dest);

            // 0.42 miles per minute and 60000 millisecond per minute
            tm.travel_time = (long) Math.round(1.0 / 0.42 * tm.miles * 60) * 1000;
        }
        return tm;
    }

    public static double getHaversine(final Location loc1, final Location loc2) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(loc2.latitude - loc1.latitude);
        double dLng = Math.toRadians(loc2.longitude - loc1.longitude);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(loc1.latitude)) * Math.cos(Math.toRadians(loc2.latitude));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist;
    }

    /* convert miles to latitude and longitude */
    public static Location getLatLngInMiles(final Location loc, final double miles) {
        Location l = new Location();
        l.latitude = miles / 69.172;
        l.longitude = l.latitude / Math.cos(loc.latitude);
        return l;
    }

    /* compute diagonal distance */
    public static double getDiagonalDistance(final Location loc1, final Location loc2) {
        double mile_sq = Math.pow((loc1.latitude - loc2.latitude) * 69.172, 2);
        mile_sq += Math.pow((loc1.longitude * Math.cos(loc1.longitude) - loc2.longitude * Math.cos(loc2.longitude)) * 69.172, 2);
        return Math.sqrt(mile_sq);
    }
    
}
