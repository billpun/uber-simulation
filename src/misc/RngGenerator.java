/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.random.MersenneTwister;

/**
 *
 * @author cpun1
 */
public class RngGenerator {

    private final double SEPS = 0.001;
    public final MersenneTwister mt;

    public RngGenerator(int seed) {
        mt = new MersenneTwister(seed);
    }

    public MersenneTwister getRngEngine() {
        return mt;
    }

    public double getUniform() {
        return mt.nextDouble();
    }

    public double getNextGamma(double mean, double std) {
        if (mean > SEPS && std > SEPS) {
            double var = std * std;
            double shape = mean * mean / var;
            double scale = var / mean;
            GammaDistribution d = new GammaDistribution(mt, shape, scale);
            return d.sample();
        } else {
            return Math.max(mean, 0);
        }
    }

    public double getNextPoisson(double mean) {
        if (mean > SEPS) {
            PoissonDistribution d = new PoissonDistribution(mt, mean, SEPS, 100);
            return d.sample();
        }
        return 0.0;
    }

    public double getNextExponential(double mean) {
        if (mean > SEPS) {
            ExponentialDistribution d = new ExponentialDistribution(mt, mean);
            return d.sample();
        }
        return 0.0;
    }

    public double getNextNormal(double mean, double std) {
        if (std > SEPS) {
            NormalDistribution d = new NormalDistribution(mt, mean, std);
            return d.sample();
        }
        return mean;
    }
}
