/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

/**
 *
 * @author cpun1
 */
public class Enums {

    public static String toString(Enum[] enums) {
        StringBuilder sb = new StringBuilder();
        char dlim = '\t';
        for (Enum e : enums) {
            sb.append(e.name()).append(dlim);
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public static enum RUN_MODE {

        generate_input,
        simulate_requests,
        simulate_drivers,
        compute_kpis
    }

    public static enum POLICY {

        nearest
    }

    public static enum HISTORICAL_REQUEST {

        request_id,
        rider_id,
        driver_id,
        driver_lat,
        driver_lng,
        origin_lat,
        origin_lng,
        request_datetime,
        destination_lat,
        destination_lng,
        delivery_datetime,
        pickup_time,
        pickup_miles,
        charges,
        miles
    }

    public static enum DRIVER_LOCATION {

        dl_id,
        latitude,
        longitude,
        datetime,
        driver_id,
        request_id
    }

    public static enum DRIVER {

        driver_id,
        first_name,
        last_name
    }

    public static enum RIDER {

        rider_id,
        first_name,
        last_name
    }

    public static enum REPORT_STATS {

        total_revenue,
        average_revenue,
        number_of_requests,
        number_of_drivers,
        travel_time,
        travel_miles,
        pickup_time,
        pickup_miles
    }
}
