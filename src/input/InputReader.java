/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import au.com.bytecode.opencsv.CSVReader;
import base.Driver;
import base.Location;
import event.Request;
import base.Rider;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import misc.Configurer;
import misc.Timing;
import uber.Uber;

/**
 *
 * @author cpun1
 */
public class InputReader {

    // input containers
    public List<Request> requests;
    public List<Driver> drivers;
    public List<Rider> riders;

    // input filenames
    private final String rider_file;
    private final String driver_file;
    private final String request_file;

    public InputReader() {
        this.rider_file = Configurer.rider_file;
        this.driver_file = Configurer.driver_file;
        this.request_file = Configurer.request_file;
    }

    public void run() {
        this.read_riders();
        this.read_drivers();
        this.read_requests();
    }

    private void read_riders() {
        Timing timer = new Timing();
        riders = new ArrayList();
        try {
            CSVReader in = new CSVReader(new FileReader(this.rider_file), '\t');
            String[] s = in.readNext();
            while ((s = in.readNext()) != null) {
                Rider rider = new Rider();
                int i = 0;
                rider.id = Integer.valueOf(s[i++].trim());
                rider.first_name = s[i++].trim();
                rider.last_name = s[i++].trim();
                riders.add(rider);
            }
            in.close();
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
        Uber.LOGGER.log(Level.INFO, "Finished reading riders. Took {0}s.", timer.getSec());
    }

    private void read_drivers() {
        Timing timer = new Timing();
        drivers = new ArrayList();
        try {
            CSVReader in = new CSVReader(new FileReader(this.driver_file), '\t');
            String[] s = in.readNext();
            while ((s = in.readNext()) != null) {
                Driver driver = new Driver();
                int i = 0;
                driver.id = Integer.valueOf(s[i++].trim());
                driver.first_name = s[i++].trim();
                driver.last_name = s[i++].trim();
                drivers.add(driver);
            }
            in.close();
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
        Uber.LOGGER.log(Level.INFO, "Finished reading drivers. Took {0}s.", timer.getSec());
    }

    private void read_requests() {
        Timing timer = new Timing();
        requests = new ArrayList();
        try {
            CSVReader in = new CSVReader(new FileReader(this.request_file), '\t');
            String[] s = in.readNext();
            while ((s = in.readNext()) != null) {
                Request request = new Request(s);
                request.rider = riders.get(request.rider.id);
                request.driver = drivers.get(request.driver.id);
                requests.add(request);
            }
            in.close();
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
        Uber.LOGGER.log(Level.INFO, "Finished reading requests. Took {0}s.", timer.getSec());
    }
}
