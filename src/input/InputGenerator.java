/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import base.Location;
import event.Request;
import base.DistanceInfo;
import base.Driver;
import base.DispatchPolicy;
import base.Rider;
import base.TimeBucket;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import misc.Configurer;
import misc.Enums;
import misc.Timing;
import misc.Utilities;
import org.apache.commons.math3.random.MersenneTwister;
import uber.Uber;

/**
 *
 * @author cpun1
 */
public class InputGenerator {

    // random number generator
    private final MersenneTwister mt;

    // configuration parameters
    private final int number_of_requests;
    private final Location coord0;
    private final double latitude_ub;
    private final double max_pickup_distance;

    public final long ONE_MINUTE_IN_MILLIS;

    // output files
    public final String time_dist_file;
    public final String request_file;
    public final String rider_file;
    public final String driver_file;

    public InputGenerator() {

        this.ONE_MINUTE_IN_MILLIS = Configurer.ONE_MINUTE_IN_MILLIS;

        // output files
        this.time_dist_file = Configurer.time_dist_file;
        this.request_file = Configurer.request_file;
        this.rider_file = Configurer.rider_file;
        this.driver_file = Configurer.driver_file;

        // get number of requests to generate
        this.number_of_requests = Configurer.number_of_requests;

        // get initial coordinate
        this.coord0 = Configurer.coord0;

        // get diagonal distance
        this.latitude_ub = Configurer.latitude_margin;

        // get maximum pickup distance
        this.max_pickup_distance = Configurer.max_pickup_distance;

        // initialize random number generator
        this.mt = new MersenneTwister(0);
    }

    public void run() {
        try {
            Timing timer = new Timing();

            // get time buckets
            List<TimeBucket> time_bkts = this.read_time_distribution();

            // sort them by from time
            Collections.sort(time_bkts, new Comparator<TimeBucket>() {
                @Override
                public int compare(TimeBucket t1, TimeBucket t2) {
                    if (t1.from < t2.from) {
                        return -1;
                    } else if (t1.from == t2.from) {
                        return 0;
                    }
                    return 1;
                }
            });

            // extract the probability
            List<Double> probs = new ArrayList();
            for (TimeBucket t : time_bkts) {
                probs.add(t.stats.mean);
            }

            // get today's date
            Calendar today = Calendar.getInstance();
            today.set(Calendar.HOUR_OF_DAY, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.MILLISECOND, 0);
            long _today = today.getTimeInMillis();

            // generate historical requests
            List<Request> requests = new ArrayList();
            while (requests.size() < this.number_of_requests) {

                // create a request
                Request request = new Request();

                // randomize its origin and destination
                request.origin = this.coord0.getRandomLocation(this.latitude_ub, mt);
                request.destination = this.coord0.getRandomLocation(this.latitude_ub, mt);

                // randomize its request time
                TimeBucket t = time_bkts.get(Utilities.multinomial(probs, mt));
                request.origin.datetime = (long) (_today + t.getRandomizedTime(mt));

                // get travel metrics
                DistanceInfo tm = Utilities.getDistanceInfo(request.origin, request.destination);

                // ignore request that doesn't travel much
                if (tm.miles >= 0.5) {

                    // get delivery time (request time + pick up time + travel time)
                    request.destination.datetime = (long) (request.origin.datetime + tm.travel_time);

                    // get miles
                    request.miles = tm.miles;

                    // get charges
                    request.charges = Uber.cost(tm.travel_time, tm.miles);

                    // add to requests
                    requests.add(request);
                }
            }

            // print out
            Uber.LOGGER.log(Level.INFO, "Finished generating requests. Took {0}s.", timer.getSec());
            timer.reset();

            // sort requests by request time 
            Collections.sort(requests, new Comparator<Request>() {
                @Override
                public int compare(Request r1, Request r2) {
                    if (r1.origin.datetime < r2.origin.datetime) {
                        return - 1;
                    } else if (r1.origin.datetime == r2.origin.datetime) {
                        return 0;
                    }
                    return 1;
                }
            });

            // assign request id and create riders
            int id = 0;
            int rider_id = 0;
            List<Rider> riders = new ArrayList();
            for (int r = 0; r < requests.size(); ++r) {
                Request request = requests.get(r);

                request.id = id++;

                // create a rider
                Rider rider = new Rider();
                rider.id = rider_id++;
                rider.first_name = this.getFirstName();
                rider.last_name = this.getLastName();
                request.rider = rider;

                // add rider to container
                riders.add(rider);
            }

            // print out
            Uber.LOGGER.log(Level.INFO, "Finished generating riders. Took {0}s.", timer.getSec());
            timer.reset();

            // assign drivers
            DispatchPolicy dispatch_policy = new DispatchPolicy(mt);
            List<Driver> drivers = new ArrayList();
            int driver_id = 0;
            for (int r = 0; r < requests.size(); ++r) {

                // get the request
                Request request = requests.get(r);

                // find the nearest driver
                Driver driver = dispatch_policy.findDriver(request, drivers);

                // if nearest driver is not found
                if (driver == null) {

                    // create a new driver
                    driver = new Driver();
                    driver.id = driver_id++;
                    driver.first_name = this.getFirstName();
                    driver.last_name = this.getLastName();
                    driver.release_location = request.origin.getRandomLocation(this.max_pickup_distance, mt);
                    driver.release_location.datetime = request.origin.datetime;
                    driver.current_location = new Location(driver.release_location);
                    drivers.add(driver);
                }

                // assign driver
                request.driver = driver;
                request.driver_location = new Location(driver.current_location);

                // determine pickup time based on origin of the request and driver's current location
                request.pickup_info = Utilities.getDistanceInfo(request.origin, request.driver_location);

                // update delivery date time with pick up time
                request.destination.datetime += request.pickup_info.travel_time;

                // update driver release location and time with request delivery location and time
                driver.release_location = new Location(request.destination);
            }

            Uber.LOGGER.log(Level.INFO, "Finished generating drivers. Took {0}s.", timer.getSec());
            timer.reset();

            // initialize a writer 
            CSVWriter out;

            // write requests
            out = new CSVWriter(new FileWriter(this.request_file),
                    '\n', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
            out.writeNext(Enums.toString(Enums.HISTORICAL_REQUEST.values()));
            for (int r = 0; r < requests.size(); ++r) {
                Request request = requests.get(r);
                out.writeNext(request.toString());
            }
            out.close();
            Uber.LOGGER.log(Level.INFO, "Finished writing requests. Took {0}s.", timer.getSec());
            timer.reset();

            // write riders
            out = new CSVWriter(new FileWriter(this.rider_file),
                    '\n', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
            out.writeNext(Enums.toString(Enums.RIDER.values()));
            for (int r = 0; r < riders.size(); ++r) {
                Rider rider = riders.get(r);
                out.writeNext(rider.toString());
            }
            out.close();
            Uber.LOGGER.log(Level.INFO, "Finished writing riders. Took {0}s.", timer.getSec());
            timer.reset();

            // write drivers
            out = new CSVWriter(new FileWriter(this.driver_file),
                    '\n', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
            out.writeNext(Enums.toString(Enums.DRIVER.values()));
            for (int r = 0; r < drivers.size(); ++r) {
                Driver driver = drivers.get(r);
                out.writeNext(driver.toString());
            }
            out.close();
            Uber.LOGGER.log(Level.INFO, "Finished writing drivers. Took {0}s.", timer.getSec());
            timer.reset();

        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    /* read default time distribution for generating historical requests */
    private List<TimeBucket> read_time_distribution() {
        Timing timer = new Timing();
        List<TimeBucket> time_bkts = null;
        try {
            // initialize the list
            time_bkts = new ArrayList();

            // initialize the reader
            CSVReader in = new CSVReader(new FileReader(this.time_dist_file), '\t');

            // read data
            String[] s = in.readNext();
            while ((s = in.readNext()) != null) {
                TimeBucket t = new TimeBucket();
                t.from = Long.valueOf(s[0].trim()) * this.ONE_MINUTE_IN_MILLIS;
                t.to = Long.valueOf(s[1].trim()) * this.ONE_MINUTE_IN_MILLIS;
                t.stats.mean = Double.valueOf(s[2].trim());

                // add top bucket
                time_bkts.add(t);
            }

            // close the connection
            in.close();
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }

        // print out
        Uber.LOGGER.log(Level.INFO, "Finished reading input time distribution. Took {0}s.", timer.getSec());
        return time_bkts;
    }

    private String getFirstName() {
        return Utilities.multinomial(firstNames, mt);

    }

    private String getLastName() {
        return Utilities.multinomial(lastNames, mt);
    }

    String[] lastNames = new String[]{
        "Smith", "Johnson", "Williams",
        "Jones", "Brown", "Davis",
        "Miller", "Wilson", "Moore",
        "Taylor", "Anderson", "White",
        "Harris", "Martin", "Thompson",
        "Garcia", "Martinez", "Robinson",
        "Clark", "Rodriguez", "Lewis",
        "Lee", "Walker", "Hall"
    };

    String[] firstNames = new String[]{
        "James", "John", "Robert",
        "Michael", "William", "David",
        "Richard", "Charles", "Joseph",
        "Thomas", "Christopher", "Daniel",
        "Mary", "Patricia", "Linda",
        "Barbara", "Elizabeth", "Jennifer",
        "Maria", "Susan", "Margaret",
        "Dorothy", "Lisa", "Nancy"
    };
}
