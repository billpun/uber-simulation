/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uber;

import au.com.bytecode.opencsv.CSVWriter;
import base.Demand;
import base.DistanceInfo;
import event.Request;
import misc.RngGenerator;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import misc.Configurer;
import misc.Enums;
import misc.Timing;
import misc.Utilities;

/**
 *
 * @author cpun1
 */
public class RequestSimulator {

    private final Map<String, Demand> demands;
    private final int number_of_streams;

    public RequestSimulator(Map<String, Demand> demands) {
        this.demands = demands;
        this.number_of_streams = Configurer.number_of_streams;
    }

    public void run() {
        // initialize global timer
        Timing timer = new Timing();

        // simulate requests
        this.simulateRequests();

        // print out
        Uber.LOGGER.log(Level.INFO,
                "All done. Took {0}s. Please go to ./requests/ to browse generated simualted requests.",
                new Object[]{timer.getSec()});
    }

    private void simulateRequests() {
        try {
            // remove all requests
            Utilities.removeFiles("./requests/");

            // get today
            Calendar today = Calendar.getInstance();
            today.set(Calendar.HOUR_OF_DAY, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.MILLISECOND, 0);
            long _today = today.getTimeInMillis();

            // for each stream
            for (int k = 0; k < this.number_of_streams; ++k) {

                // initialize the timer
                Timing _timer = new Timing();

                // initialize the random number generator with a seed
                RngGenerator rng = new RngGenerator(k);

                // initialize the requst list
                List<Request> requests = new ArrayList();

                // simulate
                for (Demand demand : demands.values()) {

                    if (demand.stats.mean <= 0) {
                        continue;
                    }

                    // simulate number of requests
                    double N = Math.round(rng.getNextGamma(demand.stats.mean, demand.stats.std));

                    // for each request, simulate origin, destination, and request time
                    int n = 0;
                    while (n < N) {
                        Request request = new Request();

                        // simulate origin
                        request.origin.latitude = rng.getNextNormal(demand.from_centroid.latitude, demand.from_spread.latitude);
                        request.origin.longitude = rng.getNextNormal(demand.from_centroid.longitude, demand.from_spread.longitude);

                        // simulate destination
                        request.destination.latitude = rng.getNextNormal(demand.to_centroid.latitude, demand.to_spread.latitude);
                        request.destination.longitude = rng.getNextNormal(demand.to_centroid.longitude, demand.to_spread.longitude);

                        // compute distance and travel time
                        DistanceInfo tm = Utilities.getDistanceInfo(request.origin, request.destination);

                        // update request and delivery time
                        request.origin.datetime = _today + demand.histogram.getRandomTime(rng.getRngEngine());
                        request.destination.datetime = request.origin.datetime + tm.travel_time;

                        // get miles
                        request.miles = tm.miles;

                        // get charges
                        request.charges = Uber.cost(tm.travel_time, tm.miles);

                        // add to request container
                        if (request.miles >= 0.5) {
                            requests.add(request);
                            n++;
                        }
                    }
                }

                // sort simulated requests
                Collections.sort(requests, new Comparator<Request>() {
                    @Override
                    public int compare(Request r1, Request r2) {
                        if (r1.origin.datetime < r2.origin.datetime) {
                            return - 1;
                        } else if (r1.origin.datetime == r2.origin.datetime) {
                            return 0;
                        }
                        return 1;
                    }
                });

                // assign request id
                int id = 0;
                for (Request request : requests) {
                    request.id = id++;
                }

                // print out the steam
                CSVWriter out = new CSVWriter(new FileWriter("./requests/stream_" + k + ".txt"),
                        '\t', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
                out.writeNext(Enums.toString(Enums.HISTORICAL_REQUEST.values()));
                for (Request request : requests) {
                    out.writeNext(request.toString());
                }
                out.close();

                Uber.LOGGER.log(Level.INFO, "Finished simulating stream {0}. Took {1}s.",
                        new Object[]{k, _timer.getSec()});
            }
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }
}
