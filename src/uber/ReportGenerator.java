/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uber;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import base.StatsUnit;
import event.Request;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import misc.Configurer;
import misc.Enums;
import misc.Timing;
import misc.Utilities;

/**
 *
 * @author cpun1
 */
public class ReportGenerator {

    private final long ONE_MINUTE_IN_MILLIS;
    private StatsUnit[] stats;

    public ReportGenerator() {
        this.ONE_MINUTE_IN_MILLIS = Configurer.ONE_MINUTE_IN_MILLIS;
    }

    public void run() {
        // initialize global timer
        Timing timer = new Timing();

        // compute KPIs
        this.computeKPIs();
        
        // generate reports
        this.printReport();

        // print out
        Uber.LOGGER.log(Level.INFO, 
                "All done. Took {0}s. Please go to ./reports/ to browse generated reports.", timer.getSec());
    }

    private void computeKPIs() {
        try {
            Timing timer = new Timing();

            // get all event list from the events directory
            List<String> files = Utilities.getAllFileNames("./events/", true);
            int number_of_eventlists = files.size();

            int size = Enums.REPORT_STATS.values().length;
            StatsUnit[][] _stats = new StatsUnit[number_of_eventlists][size];

            // for each event list
            for (int k = 0; k < number_of_eventlists; ++k) {

                // initialize all statistic containers
                for (int i = 0; i < size; ++i) {
                    _stats[k][i] = new StatsUnit();
                }

                // initialize container
                CSVReader in = new CSVReader(new FileReader(files.get(k)), '\t');
                String[] s = in.readNext();

                Set<Integer> driver_ids = new HashSet();
                double num_requests = 0.0;
                double total_charges = 0.0;
                while ((s = in.readNext()) != null) {
                    Request request = new Request(s);

                    if (!driver_ids.contains(request.driver.id)) {
                        driver_ids.add(request.driver.id);
                    }

                    _stats[k][Enums.REPORT_STATS.average_revenue.ordinal()].mean += request.charges;
                    _stats[k][Enums.REPORT_STATS.average_revenue.ordinal()].std += Math.pow(request.charges, 2);

                    double _time = request.destination.datetime - request.origin.datetime - request.pickup_info.travel_time;
                    _time /= this.ONE_MINUTE_IN_MILLIS;
                    _stats[k][Enums.REPORT_STATS.travel_time.ordinal()].mean += _time;
                    _stats[k][Enums.REPORT_STATS.travel_time.ordinal()].std += Math.pow(_time, 2);

                    double _miles = Utilities.getDistanceInfo(request.origin, request.destination).miles;
                    _stats[k][Enums.REPORT_STATS.travel_miles.ordinal()].mean += _miles;
                    _stats[k][Enums.REPORT_STATS.travel_miles.ordinal()].std += Math.pow(_miles, 2);

                    _time = request.pickup_info.travel_time;
                    _time /= this.ONE_MINUTE_IN_MILLIS;
                    _stats[k][Enums.REPORT_STATS.pickup_time.ordinal()].mean += _time;
                    _stats[k][Enums.REPORT_STATS.pickup_time.ordinal()].std += Math.pow(_time, 2);

                    _stats[k][Enums.REPORT_STATS.pickup_miles.ordinal()].mean += request.pickup_info.miles;
                    _stats[k][Enums.REPORT_STATS.pickup_miles.ordinal()].std += Math.pow(request.pickup_info.miles, 2);

                    total_charges += request.charges;
                    num_requests++;
                }
                _stats[k][Enums.REPORT_STATS.total_revenue.ordinal()].mean = total_charges;
                _stats[k][Enums.REPORT_STATS.total_revenue.ordinal()].std = Math.pow(total_charges, 2);

                _stats[k][Enums.REPORT_STATS.number_of_requests.ordinal()].mean = num_requests;
                _stats[k][Enums.REPORT_STATS.number_of_requests.ordinal()].std = Math.pow(num_requests, 2);

                _stats[k][Enums.REPORT_STATS.number_of_drivers.ordinal()].mean = driver_ids.size();
                _stats[k][Enums.REPORT_STATS.number_of_drivers.ordinal()].std = Math.pow(driver_ids.size(), 2);

                // close connection
                in.close();
            }

            // initialize all statistics
            stats = new StatsUnit[Enums.REPORT_STATS.values().length];
            for (int i = 0; i < size; ++i) {
                stats[i] = new StatsUnit();
            }

            // add all means and sum of squares together
            for (int k = 0; k < number_of_eventlists; ++k) {
                for (int i = 0; i < size; ++i) {
                    // sum all the means
                    stats[i].mean += _stats[k][i].mean;

                    // pull all sum of squares together
                    stats[i].std += _stats[k][i].std;
                }
            }

            // compute average and standard deviation
            double divisor = stats[Enums.REPORT_STATS.number_of_requests.ordinal()].mean;
            for (int i = 0; i < size; ++i) {
                double _divisor = divisor;
                if (i == Enums.REPORT_STATS.total_revenue.ordinal()
                        || i == Enums.REPORT_STATS.number_of_requests.ordinal()
                        || i == Enums.REPORT_STATS.number_of_drivers.ordinal()) {
                    _divisor = number_of_eventlists;
                }
                stats[i].std = Math.sqrt(stats[i].std / _divisor - Math.pow(stats[i].mean / _divisor, 2));
                stats[i].mean /= _divisor;
            }

            // print outs
            Uber.LOGGER.log(Level.INFO, "Finished computing KPIs. Took {0}s.", timer.getSec());

        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    private void printReport() {
        try {
            // initialize timer
            Timing timer = new Timing();

            Calendar today = Calendar.getInstance();
            String now = Utilities.long2date(today.getTimeInMillis());
            CSVWriter out = new CSVWriter(new FileWriter(
                    "./reports/report_" + now.replace(":", "-").replace("/", "-") + ".txt"),
                    '\n', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);

            out.writeNext("------------------------------ Simulation Report ------------------------------ ");
            out.writeNext();
            out.writeNext("Compiled on " + now);
            out.writeNext();
            // print KPIs
            out.writeNext("* All reported with mean and standard deviation");
            out.writeNext("* Times are in minutes");
            out.writeNext("* Distance are in miles");
            out.writeNext();
            out.writeNext("------------------------------ Summary ------------------------------ ");
            out.writeNext();
            out.writeNext("\tTotal revenue                        " + stats[Enums.REPORT_STATS.total_revenue.ordinal()].toString());
            out.writeNext("\tAverage revenue                      " + stats[Enums.REPORT_STATS.average_revenue.ordinal()].toString());
            out.writeNext("\tNumber of drivers needed             " + stats[Enums.REPORT_STATS.number_of_drivers.ordinal()].toString());
            out.writeNext("\tNumber of requests                   " + stats[Enums.REPORT_STATS.number_of_requests.ordinal()].toString());
            out.writeNext("\tAverage traveled time (w/o pickup)   " + stats[Enums.REPORT_STATS.travel_time.ordinal()].toString());
            out.writeNext("\tAverage traveled miles (w/o pickup)  " + stats[Enums.REPORT_STATS.travel_miles.ordinal()].toString());
            out.writeNext("\tAverage pickup time                  " + stats[Enums.REPORT_STATS.pickup_time.ordinal()].toString());
            out.writeNext("\tAverage pickup miles                 " + stats[Enums.REPORT_STATS.pickup_miles.ordinal()].toString());
            out.writeNext();

            // print configurations
            out.writeNext("------------------------------ Configuration ------------------------------ ");
            out.writeNext();
            CSVReader in = new CSVReader(new FileReader(Configurer.filename), '\n');
            String[] s = in.readNext();
            while ((s = in.readNext()) != null) {
                out.writeNext(s);
            }
            out.writeNext("------------------------------ End ------------------------------ ");

            out.close();

            // print out
            Uber.LOGGER.log(Level.INFO, "Finish generating reports. Took {0}s.", timer.getSec());

        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }
}
