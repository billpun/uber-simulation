/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uber;

import base.Demand;
import base.Grid;
import base.Histogram;
import event.Request;
import base.StatsUnit;
import base.TimeBucket;
import input.InputReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import misc.Configurer;
import misc.Timing;

/**
 *
 * @author cpun1
 */
public class GridGenerator {

    private final InputReader input_reader;
    private final int number_of_grids;

    // list of grids
    private List<Grid> grids;

    // list of demands
    public Map<String, Demand> demands;

    // geographical boundaries
    private double min_latitude;
    private double max_latitude;
    private double min_longitude;
    private double max_longitude;

    private double lat_inc;
    private double lng_inc;

    public GridGenerator(InputReader input_reader) {
        this.input_reader = input_reader;
        this.number_of_grids = Configurer.number_of_grids;
    }

    public void run() {

        // generate grids
        this.generate_grids();

        // compute statistics for request simlation
        this.compute_statistics();
    }

    private void generate_grids() {
        try {
            Timing timer = new Timing();

            min_latitude = 999999;
            max_latitude = -999999;
            min_longitude = 999999;
            max_longitude = -999999;

            // determine maximum and minimum latitude and longitude
            for (Request request : this.input_reader.requests) {
                if (min_latitude >= request.origin.latitude) {
                    min_latitude = request.origin.latitude;
                }
                if (min_latitude >= request.destination.latitude) {
                    min_latitude = request.destination.latitude;
                }
                if (min_latitude >= request.driver_location.latitude) {
                    min_latitude = request.driver_location.latitude;
                }

                if (max_latitude <= request.origin.latitude) {
                    max_latitude = request.origin.latitude;
                }
                if (max_latitude <= request.destination.latitude) {
                    max_latitude = request.destination.latitude;
                }
                if (max_latitude <= request.driver_location.latitude) {
                    max_latitude = request.driver_location.latitude;
                }

                if (min_longitude >= request.origin.longitude) {
                    min_longitude = request.origin.longitude;
                }
                if (min_longitude >= request.destination.longitude) {
                    min_longitude = request.destination.longitude;
                }
                if (min_longitude >= request.driver_location.longitude) {
                    min_longitude = request.driver_location.longitude;
                }

                if (max_longitude <= request.origin.longitude) {
                    max_longitude = request.origin.longitude;
                }
                if (max_longitude <= request.destination.longitude) {
                    max_longitude = request.destination.longitude;
                }
                if (max_longitude <= request.driver_location.longitude) {
                    max_longitude = request.driver_location.longitude;
                }
            }

            // enlarge the grid by 10%
            double lat_margin = (max_latitude - min_latitude) * 0.1;
            double lng_margin = (max_longitude - min_longitude) * 0.1;

            // add the margins back
            max_latitude += lat_margin;
            min_latitude -= lat_margin;

            max_longitude += lng_margin;
            min_longitude -= lng_margin;

            // compute grid increment
            this.lat_inc = (max_latitude - min_latitude) / this.number_of_grids;
            this.lng_inc = (max_longitude - min_longitude) / this.number_of_grids;

            // generate grids
            int grid_id = 0;
            grids = new ArrayList();
            for (int i = 0; i < this.number_of_grids; ++i) {
                for (int j = 0; j < this.number_of_grids; ++j) {
                    Grid grid = new Grid();
                    grid.id = grid_id++;
                    grid.left_latitude = min_latitude + i * lat_inc;
                    grid.right_latitude = grid.left_latitude + lat_inc;
                    grid.bottom_longitude = min_longitude + j * lng_inc;
                    grid.top_longitude = grid.bottom_longitude + lng_inc;
                    grids.add(grid);
                }
            }

            // print out
            Uber.LOGGER.log(Level.INFO, "Finished generating grids. Took {0}s.", timer.getSec());

        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    private void compute_statistics() {
        try {
            Timing timer = new Timing();

            // collect requests for each demand unit
            this.demands = new HashMap();
            for (int r = 0; r < this.input_reader.requests.size(); ++r) {
                Request request = this.input_reader.requests.get(r);

                // search for the grid pair that the request belongs
                Grid from = null;
                Grid to = null;
                for (Grid grid : grids) {
                    if (grid.isOriginInGrid(request)) {
                        from = grid;
                    }
                    if (grid.isDestinInGrid(request)) {
                        to = grid;
                    }
                    if (from != null && to != null) {
                        break;
                    }
                }

                // create demand
                Demand demand = new Demand();
                demand.from = from;
                demand.to = to;
                demand.setID();

                // add request to the corresponding demand
                if (this.demands.containsKey(demand.getID())) {
                    this.demands.get(demand.getID()).requests.add(request);
                } else {
                    demand.requests.add(request);
                    this.demands.put(demand.getID(), demand);
                }
            }

            // compute statistics (everything is averaged over days)
            for (Demand demand : this.demands.values()) {

                // initialize daily statistics 
                // key: date, value: statistics
                Map<Long, StatsUnit> stats = new HashMap();

                // key: date, value: histogram (time distribution)
                Map<Long, Histogram> histograms = new HashMap();

                double size = demand.requests.size();
                for (Request request : demand.requests) {

                    // get datetime based on the request datetime
                    Calendar day = Calendar.getInstance();
                    day.setTimeInMillis(request.origin.datetime);

                    day.set(Calendar.HOUR_OF_DAY, 0);
                    day.set(Calendar.SECOND, 0);
                    day.set(Calendar.MINUTE, 0);
                    day.set(Calendar.MILLISECOND, 0);

                    // get only the date without hours/minutes/seconds
                    long date = day.getTimeInMillis();

                    // collect statistics (adding one request to the mean)
                    if (stats.containsKey(date)) {
                        stats.get(date).mean += 1;
                    } else {
                        StatsUnit su = new StatsUnit();
                        su.mean += 1;
                        stats.put(date, su);
                    }

                    // NOTE: note that I have ignored the correlation between latitude and longitude
                    // there are better ways to do this
                    demand.from_centroid.latitude += request.origin.latitude;
                    demand.from_spread.latitude += Math.pow(request.origin.latitude, 2);

                    demand.from_centroid.longitude += request.origin.longitude;
                    demand.from_spread.longitude += Math.pow(request.origin.longitude, 2);

                    demand.to_centroid.latitude += request.destination.latitude;
                    demand.to_spread.latitude += Math.pow(request.destination.latitude, 2);

                    demand.to_centroid.longitude += request.destination.longitude;
                    demand.to_spread.longitude += Math.pow(request.destination.longitude, 2);

                    // compute histogram
                    long time = request.origin.datetime - day.getTimeInMillis();
                    if (histograms.containsKey(date)) {

                        // add one request to the corresponding time bucket
                        histograms.get(date).add(time, 1);
                    } else {

                        Histogram histogram = new Histogram();
                        histogram.add(time, 1);
                        histograms.put(date, histogram);
                    }
                }

                // compute origin mean
                demand.from_centroid.latitude /= size;
                demand.from_centroid.longitude /= size;

                // compute destination mean
                demand.to_centroid.latitude /= size;
                demand.to_centroid.longitude /= size;

                // compute origin standard deviation
                demand.from_spread.latitude = Math.sqrt(demand.from_spread.latitude / size - Math.pow(demand.from_centroid.latitude, 2));
                demand.from_spread.longitude = Math.sqrt(demand.from_spread.longitude / size - Math.pow(demand.from_centroid.longitude, 2));

                // compute destination standard deviation
                demand.to_spread.latitude = Math.sqrt(demand.to_spread.latitude / size - Math.pow(demand.to_centroid.latitude, 2));
                demand.to_spread.longitude = Math.sqrt(demand.to_spread.longitude / size - Math.pow(demand.to_centroid.longitude, 2));

                // compute request mean and standard deviation
                size = stats.size();
                for (Map.Entry<Long, StatsUnit> item : stats.entrySet()) {
                    demand.stats.mean += item.getValue().mean;
                    demand.stats.std += Math.pow(item.getValue().mean, 2);
                }
                demand.stats.mean /= size;
                demand.stats.std = Math.sqrt(demand.stats.std / size - Math.pow(demand.stats.mean, 2));

                // compute histogram (mean and std for each time bucket)
                size = histograms.size();
                for (Map.Entry<Long, Histogram> item : histograms.entrySet()) {
                    for (int i = 0; i < demand.histogram.size(); ++i) {
                        Histogram _hist = item.getValue();
                        TimeBucket timebkt = demand.histogram.get(i);
                        timebkt.stats.mean += _hist.get(i).stats.mean;
                        timebkt.stats.std += Math.pow(_hist.get(i).stats.mean, 2);
                    }
                }
                for (TimeBucket t : demand.histogram.time_bkts) {
                    t.stats.mean /= size;
                    t.stats.std = Math.sqrt(t.stats.std / size - Math.pow(t.stats.mean, 2));
                }
            }

            //print out
            Uber.LOGGER.log(Level.INFO, "Finished computing statistics. Took {0}s.", timer.getSec());
            
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }
}
