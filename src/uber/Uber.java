/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uber;

import input.InputGenerator;
import input.InputReader;
import java.util.logging.Logger;
import misc.Configurer;
import misc.Enums;
import misc.Utilities;

/**
 *
 * @author cpun1
 */
public class Uber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // run uber simulation engine
        Uber ub = new Uber();
        ub.run();
    }

    // global logger
    public static final Logger LOGGER = Logger.getLogger(
            Thread.currentThread().getStackTrace()[0].getClassName());

    private int run_mode;

    // simulation constructor
    public Uber() {
    }

    public void run() {

        // initialize
        initialize();

        // generate input data
        if (run_mode == Enums.RUN_MODE.generate_input.ordinal()) {
            InputGenerator input_generator = new InputGenerator();
            input_generator.run();
        }

        // simulate requests
        if (run_mode == Enums.RUN_MODE.simulate_requests.ordinal()) {
            InputReader input_reader = new InputReader();
            input_reader.run();

            GridGenerator grids = new GridGenerator(input_reader);
            grids.run();

            RequestSimulator simulator = new RequestSimulator(grids.demands);
            simulator.run();
        }

        // simulate drivers
        if (run_mode == Enums.RUN_MODE.simulate_drivers.ordinal()) {
            DriverSimulator simulator = new DriverSimulator();
            simulator.run();
        }

        // compute KPIs
        if (run_mode == Enums.RUN_MODE.compute_kpis.ordinal()) {
            ReportGenerator report = new ReportGenerator();
            report.run();
        }
    }

    private void initialize() {

        // read configuration
        Configurer configurer = new Configurer();
        configurer.run();

        // read run mode
        this.run_mode = Configurer.run_mode;

        // create necessary folders
        Utilities.createFolder("input");
        Utilities.createFolder("requests");
        Utilities.createFolder("events");
        Utilities.createFolder("reports");
    }

    /* Uber cost function */
    public static double cost(long travel_time, double miles) {
        return Math.max(8, 0.4 * travel_time / Configurer.ONE_MINUTE_IN_MILLIS + 2.15 * miles);
    }

}
