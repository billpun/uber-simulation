/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uber;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import base.DistanceInfo;
import base.Driver;
import event.Event;
import event.EventList;
import base.Location;
import base.DispatchPolicy;
import event.DropOff;
import event.PickUp;
import event.Request;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import misc.Configurer;
import misc.Enums;
import misc.RngGenerator;
import misc.Timing;
import misc.Utilities;

/**
 *
 * @author cpun1
 */
public class DriverSimulator {

    private final double max_pickup_distance;

    // constructor
    public DriverSimulator() {
        this.max_pickup_distance = Configurer.max_pickup_distance;
    }

    public void run() {
        // initialize global timer
        Timing timer = new Timing();

        // simulate drivers
        this.simulateDrivers();

        // print out
        Uber.LOGGER.log(Level.INFO,
                "All done. Took {0}s. Please go to ./events/ to browse generated event lists.",
                new Object[]{timer.getSec()});
    }

    private void simulateDrivers() {
        try {
            // remove all events
            Utilities.removeFiles("./events/");

            // get all files from the request directory
            List<String> files = Utilities.getAllFileNames("./requests/", true);

            // for each stream
            for (int k = 0; k < files.size(); ++k) {

                // initialize the timer
                Timing _timer = new Timing();

                // initialize the random number generator
                RngGenerator rng = new RngGenerator(k);

                // initialize the event list
                EventList event_list = new EventList();

                // initialize stream reader
                CSVReader in = new CSVReader(new FileReader(files.get(k)), '\t');
                String[] s = in.readNext();

                // for each request in the stream
                while ((s = in.readNext()) != null) {

                    // create a request
                    Request request = new Request(s);

                    // set event time
                    request.event_time = request.origin.datetime;

                    // add to event list
                    event_list.add(request);
                }
                in.close();

                // initialize driver id
                int driver_id = 0;

                // initialize policy
                DispatchPolicy dispatch_policy = new DispatchPolicy(rng.getRngEngine());

                // initialize driver container
                List<Driver> drivers = new ArrayList();

                // for each event in the event list
                while (event_list.hasNext()) {

                    // get the event
                    Event event = event_list.next();

                    // if it is a request
                    if (event instanceof Request) {

                        // convert the event to a request
                        Request request = (Request) event;

                        // find a driver by the selected policy
                        Driver driver = dispatch_policy.findDriver(request, drivers);

                        // if no driver is found
                        if (driver == null) {

                            // create a new driver
                            driver = new Driver();
                            driver.id = driver_id++;

                            // randomize its initialize location by the origin of the request
                            driver.release_location = request.origin.getRandomLocation(this.max_pickup_distance, rng.getRngEngine());
                            driver.release_location.datetime = request.origin.datetime;

                            // set its current location to the same as the release location
                            driver.current_location = new Location(driver.release_location);

                            // add to driver list
                            drivers.add(driver);
                        }

                        // assign the driver to the request
                        request.driver = driver;

                        // update driver's release location and time with the delivery location and time
                        driver.release_location = new Location(request.destination);

                        // create a pick up event
                        PickUp pickup = new PickUp();
                        pickup.driver = driver;
                        pickup.request = request;

                        // compute pickup travel time and distance
                        pickup.request.pickup_info = Utilities.getDistanceInfo(request.origin, driver.current_location);

                        // update event time of the pickup event
                        pickup.event_time = request.origin.datetime + pickup.request.pickup_info.travel_time;

                        // add a pickup event
                        event_list.add(pickup);
                    }

                    // if the event is a pickup event
                    if (event instanceof PickUp) {

                        // convert the event to a pickup event
                        PickUp pickup = (PickUp) event;

                        // simulate exact travel time and distance
                        DistanceInfo tm = Utilities.getDistanceInfo(pickup.request.origin, pickup.request.destination);

                        // create a dropoff event
                        DropOff dropoff = new DropOff();
                        dropoff.driver = pickup.driver;
                        dropoff.request = pickup.request;
                        dropoff.event_time = pickup.event_time + tm.travel_time;

                        // add to event list
                        event_list.add(dropoff);
                    }
                    if (event instanceof DropOff) {
                        DropOff dropoff = (DropOff) event;

                        // simulate exact destination (not yet implemented)
                        Location exact_destination = simulate_exact_destination(dropoff);

                        // get exact miles and charges (additional simulation)
                        DistanceInfo tm = Utilities.getDistanceInfo(dropoff.request.origin, exact_destination);
                        dropoff.request.miles = tm.miles;
                        dropoff.request.charges = Uber.cost(tm.travel_time, tm.miles);
                        dropoff.request.destination = new Location(exact_destination);
                        dropoff.request.destination.datetime = dropoff.event_time;

                        // update release location of the driver
                        dropoff.driver.release_location = new Location(exact_destination);
                        dropoff.driver.release_location.datetime = dropoff.event_time;
                    }
                }

                // print requests
                CSVWriter out = new CSVWriter(new FileWriter("./events/stream_" + k + ".txt"),
                        '\t', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
                out.writeNext(Enums.toString(Enums.HISTORICAL_REQUEST.values()));
                List<Event> events = event_list.getAll();
                for (int i = 0; i < events.size(); ++i) {
                    Event event = events.get(i);

                    // only print out request event
                    if (event instanceof Request) {
                        out.writeNext(event.toString());
                    }
                }
                out.close();

                // print out
                Uber.LOGGER.log(Level.INFO, "Finished driver simulation for stream {0}. Took {1}s.",
                        new Object[]{k, _timer.getSec()});
                _timer.reset();
            }
        } catch (Exception e) {
            Uber.LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    private Location simulate_exact_destination(DropOff dropoff) {
        return new Location(dropoff.request.destination);
    }
}
